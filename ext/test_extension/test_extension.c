#include <ruby.h>
#include "extconf.h"
#include "test_extension.h"

/* Function Prototypes ********************************************************/
VALUE method_add(VALUE self, VALUE x, VALUE y);

/* Global Variables ***********************************************************/
static VALUE Extension = Qnil;

/* Implementation *************************************************************/

/* Initialise the Ruby extension */
void Init_test_extension() {
	Extension = rb_define_module(MODULE_NAME);

	rb_define_singleton_method(Extension, "add", method_add, 2);
}

/* Returns the sum of the two provided values */
VALUE method_add(VALUE self, VALUE x, VALUE y) {
	int xValue = NUM2INT(x);
	int yValue = NUM2INT(y);

	return INT2NUM(xValue + yValue);
}
