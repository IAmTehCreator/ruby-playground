Gem::Specification.new do |s|
  s.name        = 'demo'
  s.version     = '0.0.0'
  s.date        = '2016-12-11'
  s.summary     = 'Simple demo module'
  s.description = "A simple gem containing a demo module and an extension"
  s.authors     = ['Steven Milne']
  s.email       = 'stevenimilne@outlook.com'
  s.homepage    = 'https://bitbucket.org/IAmTehCreator/'
  s.license     = 'MIT'

  s.files       = [
	  'lib/demo.rb',

	  'ext/test_extension/test_extension.c',
	  'ext/test_extension/test_extension.h'
  ]

  s.extensions = [
	  'ext/test_extension/extconf.rb'
  ]
end
