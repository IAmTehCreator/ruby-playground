require 'test_extension'

module Demo

	ME_FIRST_NAME = 'Steven'
	ME_LAST_NAME = 'Milne'

	############################################################################
	# Methods ##################################################################
	############################################################################
	def Demo.get_me
		Demo::Person.new(ME_FIRST_NAME, ME_LAST_NAME)
	end

	def Demo.add x, y
		Extension.add x, y
	end

	############################################################################
	# Classes ##################################################################
	############################################################################
	class Person

		def initialize firstname, lastname
			@firstname = firstname
			@lastname = lastname
		end

		def greet name = ''
			puts "Hello #{name}, my name is #{@firstname} #{@lastname}"
		end
	end

end
