# Ruby Playground
This repo contains a Ruby environment use for learning Ruby.

## Installing Ruby
Mac OS X has a built-in version of Ruby available but there is a more flexible Ruby environment that can be used on OSX, RVM the Ruby enVironment Manager. RVM allows you to have multiple versions of Ruby installed at any one time. The easiest way to install RVM is using homebrew, below is the Bash commands required to install RVM and it's dependencies using homebrew;

```bash
brew install phantomjs
brew install qt
\curl -sSL https://get.rvm.io | bash -s stable
rvm install 2.2.1
```

Once a Ruby has been installed you can use the `irb` command in the terminal to invoke an interactive Ruby interpreter. To make use of this repo you should also install Bundler so that the required gems can be installed. Below is the Bash commands required to install Bundler.

```bash
gem install bundler
```

## Ruby Gems

### Creating a Gem

### Installing a Gem

## Ruby Summary
